var game = {state : 'off'};
$(function() {
 game.shapes = gameInit();
});

/*
 * Рисуем всю тему
 */
function gameInit () {
	var pWidth = 630, pHeight = 408,
	midFieldX = pWidth/2,
	midFieldY = pHeight/2, //размеры поля 
	gLength = 100; //длина ворот
	var chromeIcon = "M15.318,7.677c0.071-0.029,0.148-0.046,0.229-0.046h11.949c-2.533-3.915-6.938-6.506-11.949-6.506c-5.017,0-9.428,2.598-11.959,6.522l4.291,7.431C8.018,11.041,11.274,7.796,15.318,7.677zM28.196,8.84h-8.579c2.165,1.357,3.605,3.763,3.605,6.506c0,1.321-0.334,2.564-0.921,3.649c-0.012,0.071-0.035,0.142-0.073,0.209l-5.973,10.347c7.526-0.368,13.514-6.587,13.514-14.205C29.77,13.002,29.201,10.791,28.196,8.84zM15.547,23.022c-2.761,0-5.181-1.458-6.533-3.646c-0.058-0.046-0.109-0.103-0.149-0.171L2.89,8.855c-1,1.946-1.565,4.153-1.565,6.492c0,7.624,5.999,13.846,13.534,14.205l4.287-7.425C18.073,22.698,16.848,23.022,15.547,23.022zM9.08,15.347c0,1.788,0.723,3.401,1.894,4.573c1.172,1.172,2.785,1.895,4.573,1.895c1.788,0,3.401-0.723,4.573-1.895s1.895-2.785,1.895-4.573c0-1.788-0.723-3.4-1.895-4.573c-1.172-1.171-2.785-1.894-4.573-1.894c-1.788,0-3.401,0.723-4.573,1.894C9.803,11.946,9.081,13.559,9.08,15.347z";
	//инициализация рафаэля и само поле
	paper = Raphael ("game_area", pWidth, pHeight);
	var field = paper.rect(10, 10, pWidth-20, pHeight-20);
	field.attr("stroke-width", 5);
	
	//ворота
	var gY = (pHeight - gLength -20)/2;
	
	//var p1Goal = paper.path("M10,"+gY+"V"+gLength);
	//var p2Goal = paper.path("M620,"+gY+"V"+gLength);
	var p1Goal = paper.rect(10, gY+5, 6, gLength);
	var p2Goal = paper.rect(620, gY+5, 6, gLength);
	p1Goal.attr("stroke", "#DDD");
	p2Goal.attr("stroke", "#DDD");
	p1Goal.attr("stroke-width", 6);
	p2Goal.attr("stroke-width", 6);
	
	//Ракетки игроков
	var stickLength = gLength+10,
		stickIndent = 40,
		stickY = (pHeight-20-stickLength)/2;
	
	
	var p1 = paper.rect(stickIndent, stickY, 13, stickLength+10, 3);
	var p2 = paper.rect(pWidth-10-5-stickIndent, stickY, 13, stickLength+10, 3);
	p1.attr("fill", "#000000");
	p2.attr("fill", "#000000");
	
	
	//разделительная линия
	var midHeight = pHeight-10;
	var divider = paper.path("M"+midFieldX+",10V"+midHeight);
	divider.attr("stroke-dasharray","-");
	divider.attr("stroke-width","5");
	
	//центральный круг
//	var center = paper.circle (midFieldX, midFieldY, 40);
//	center.attr("fill","#DDD");
//	center.attr("stroke-width", 5);
	
	//шарик
	//var ball = paper.path(chromeIcon).attr({fill: "#000", stroke: "none"});
	ball = paper.circle(midFieldX, midFieldY, 15);
	ball.attr("fill", "#CC0000");
	ball.attr("stroke", "#CC0000");
	ball.data('speed', 20);
	ball.data("dx", 20);
	ball.data("dy", 20);


    //game events
    eve.on("game_start", gameStart);
    eve.on("game_pause", gamePause);
    //eve.on("game_start", gameStart);

    return {
        player  : p2,
        field   : field,
        ball 	: ball,
        paper	: paper,
        stickLeft: p1,
        stickRight: p2
    }
}

function gameStart(){
    game.state = 'on';
    //readySteadyGo();
    game.loop = setInterval(moveBall, 50);
    //moveBall();
    playerEventsBind();
}

function readySteadyGo(){
	var paper = game.shapes.paper;
	var delay = 3;
}

function moveBall(){
	var ball = game.shapes.ball;
	var field = game.shapes.field;
	var speed = ball.data("speed");
	var p = game.shapes.paper;
	var dx = ball.data("dx"), dy = ball.data("dy");
	var r = ball.attrs.r, cx = ball.attrs.cx,
		cy = ball.attrs.cy;
	
	var p1 = game.shapes.stick1, 
		p2 = game.shapes.stick2;
	
	if (cx+r-dx <=35 || cx+r+dx >=field.attrs.width-15){
		if (!ball.data("isChangedX")){
			ball.data("dx", -dx);
		}
		ball.data("isChangedX", true);
	}
	else{
		if (ball.data("isChangedX")){
			ball.data("isChangedX", false);
		}
	}

	if (cy+r+dy>field.attrs.height-12 || cy+r-dy<32){
		if (!ball.data("isChangedY")){
			ball.data("dy", -dy);
		}
		ball.data("isChangedY", true);
	}
	else {
		if (ball.data("isChangedY")){
			ball.data("isChangedY", false);
		}
	}
	
	ball.animate({'cx': ball.attrs.cx-dx, 'cy': ball.attrs.cy-dy}, 100);
}

function gamePause(){
    game.state = 'off';
    clearInterval(game.loop);
    game.loop = null;
    playerEventsUnbind();
}

function gameOver(){

}

function gameReload(){

}


function playerEventsBind(){

    var player = game.shapes.player;
    var field = game.shapes.field;
    var keys = {};
    var interval = null;

    $(document).on("keydown", function(e){
        e.preventDefault();



        keys[e.keyCode] = true;

        if (interval)
            return;
        interval = setTimeout(move,15);


    });
    $(document).on("keyup", function(e){
        e.preventDefault();

        delete keys[e.keyCode];

        if(isEmpty(keys)){
            clearTimeout(interval);
            reset();
            interval = null;
        }

    });

    function isEmpty(obj){
        for(var i in obj){
            if (obj.hasOwnProperty(i))
                return false;
        }
        return true;
    }

    function reset(){
        player.transform("r0");
    }

    function move(){
        if(keys[38] && !keys[40]){ //up
            if(player.attrs.y > 15){
                player.animate({'x': player.attrs.x, 'y': player.attrs.y-5 }, 0);
            }
        }
        if(!keys[38] && keys[40]){ //down
             if(player.attrs.y < field.attrs.height - player.attrs.height + 5){
                 player.animate({'x': player.attrs.x, 'y': player.attrs.y+5 }, 0);
             }
        }
        if(keys[37] && !keys[39]){
            player.transform("r-30");
        }
        if(!keys[37] && keys[39]){
            player.transform("r30");
        }

        if(!keys[37] && !keys[39]){
            player.transform("r0");
        }

        interval = setTimeout(move,15);
    }

}



function playerEventsUnbind(){
    $(document).off("keydown");
}

