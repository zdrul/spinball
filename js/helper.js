$(function(){
   $("#game_actions").click(function(){
       var that = $(this);
       switch (game.state){
           case 'off':
               that.val('Pause');
               eve("game_start");
               break;
           case 'on':
               that.val('Play');
               eve("game_pause");
               break;
       }
   });
});
